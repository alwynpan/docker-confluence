# Atlassian Confluence Docker

## Build image

`$ docker build -t alwynpan/docker-confluence:6.9.0 .`


## Usage

### docker run

```
$ docker run --name confluence --restart=always \
             --link postgres-confluence:postgres-confluence
             -v /path/to/confluence/home:/var/atlassian/application-data/confluence \
             -p 8090:8090 -p 8091:8091 \
             -e -X_PROXY_NAME=<domain name> \
             -e -X_PROXY_PORT=<80/443> \
             -e -X_PROXY_SCHEME=<http/https> \
             -e -X_PATH='' -d alwynpan/docker-confluence:6.9.0
```

### docker-compose.yaml

```
version: "3"
services:
  postgres-confluence:
    image: "postgres:9.5"
    restart: always
    volumes:
      - /path/to/confluence/database:/var/lib/postgresql/data
    environment:
      - POSTGRES_PASSWORD=password
    container_name: postgres-confluence

  confluence:
    image: "alwynpan/docker-confluence:6.9.0"
    restart: always
    volumes:
      - /path/to/confluence/home:/var/atlassian/application-data/confluence
    links:
      - postgres-confluence
    ports:
      - "8090:8090"
      - "8091:8091"
    environment:
      - X_PROXY_NAME=<domain name>
      - X_PROXY_PORT=<80/443>
      - X_PROXY_SCHEME=<http/https>
      - X_PATH=
    container_name: confluence
```
